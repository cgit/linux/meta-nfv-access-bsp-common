kernel_do_deploy[vardepsexclude] = "DATETIME"
kernel_do_deploy_append () {

     #
     # Drop the regular defconfig along side the others for consistency
     #
     cp ${B}/.config ${DEPLOYDIR}/config-${PKGE}-${PKGV}-${PKGR}-${MACHINE}-${DATETIME}.config

     #
     # add symlink
     #
     ln -sf config-${PKGE}-${PKGV}-${PKGR}-${MACHINE}-${DATETIME}.config ${DEPLOYDIR}/config-${MACHINE}.config

}
